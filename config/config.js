const os = require('os')
require('dotenv').config({ path: `${os.homedir}/env/surfboard/dashboard/.env` })

module.exports = {
	PORT: process.env.PORT || 4000,
	DB_PATH: `mysql://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`,
}
