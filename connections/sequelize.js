const { Sequelize } = require('sequelize')
const { DB_PATH } = require('../config/config')

const sequelize = new Sequelize(DB_PATH, { operatorsAliases: false })

module.exports = sequelize
