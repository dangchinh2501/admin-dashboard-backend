const { snakeCase } = require('lodash')

module.exports = (obj) => {
	for (let property in obj) {
		fieldName = snakeCase(property)
		obj[property].field = fieldName
	}

	return obj
}
