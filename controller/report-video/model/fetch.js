const { ReportVideo } = require('../../../models/')

/**
 * Get the whole list of video
 * @param [rangeStart: number, rangeEnd: number] range
 * @param [field: string, order(ASC or DESC): string] sort
 * @param {object} filter
 */
const getListReportVideo = async (range, sort, filter) => {
	const [field, order] = sort
	const [start, end] = range

	return await ReportVideo.findAndCountAll({
		raw: true,
		limit: end - start + 1,
		offset: start,
		order: field ? [[field, order]] : [],
		where: filter,
	})
}

/**
 * Get one report by id
 * @param {number} id
 */
const getReportVideoById = async (id) => {
	return await ReportVideo.findByPk(id)
}

/**
 * Get users with a filter
 * @param {object} filter
 */
const getReprotVideoFilter = async (filter) => {
	return await ReportVideo.findAll({ where: filter })
}

module.exports = {
	getListReportVideo,
	getReportVideoById,
	getReprotVideoFilter,
}
