const {
	getListReportVideo,
	getReportVideoById,
	getReprotVideoFilter,
} = require('./model/fetch')

/**
 * Get the whole list of report
 * @param {[rangeStart: number, rangeEnd: number] range, [field: string, order: string] sort, {Object} filter}
 */
const getList = async (req, res, next) => {
	const { range, sort, filter } = req.query

	let videos
	try {
		videos = await getListReportVideo(
			JSON.parse(range),
			JSON.parse(sort),
			JSON.parse(filter)
		)
	} catch (error) {
		console.log(error)
	}

	res.set(
		'content-range',
		`${req.baseUrl} ${JSON.parse(range)[0]}-${JSON.parse(range)[1]}/${
			videos.count
		}`
	)

	res.json(videos.rows)
}

/**
 * Get video by Id
 * @param {id: number} req
 */
const getOne = async (req, res, next) => {
	let video = await getReportVideoById(req.params.id)

	res.json(user)
}

/**
 * Get many user with filter
 * @param {*} req
 */
const getMany = async (req, res, next) => {
	let videos

	try {
		videos = await getReprotVideoFilter(JSON.parse(req.query.filter))
	} catch (error) {
		console.log(error)
	}

	res.json(videos)
}

module.exports = { getList, getMany, getOne }
