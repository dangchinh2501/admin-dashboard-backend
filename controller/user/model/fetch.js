const { Users } = require('../../../models/')
const User = require('../../../models/User')

/**
 * Get the whole list of user
 * @param [rangeStart: number, rangeEnd: number] range
 * @param [field: string, order(ASC or DESC): string] sort
 * @param {object} filter
 */
const getListUser = async (range, sort, filter) => {
	const [field, order] = sort
	const [start, end] = range

	return await Users.findAndCountAll({
		raw: true,
		limit: end - start + 1,
		offset: start,
		order: field ? [[field, order]] : [],
		where: filter,
	})
}

/**
 * Get one user by userId
 * @param {number} id
 */
const getUserById = async (id) => {
	return await User.findByPk(id)
}

/**
 * Get users with a filter
 * @param {object} filter
 */
const getUserFilter = async (filter) => {
	return await User.findAll({ where: filter })
}

module.exports = {
	getListUser,
	getUserById,
	getUserFilter,
}
