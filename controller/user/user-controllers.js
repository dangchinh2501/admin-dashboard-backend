const { getListUser, getUserById, getUserFilter } = require('./model/fetch')
const queryString = require('querystring')

/**
 * Get the whole list of user
 * @param {[rangeStart: number, rangeEnd: number] range, [field: string, order: string] sort, {Object} filter}
 */
const getList = async (req, res, next) => {
	const { range, sort, filter } = req.query

	let users = await getListUser(
		JSON.parse(range),
		JSON.parse(sort),
		JSON.parse(filter)
	)

	res.set(
		'content-range',
		`${req.baseUrl} ${JSON.parse(range)[0]}-${JSON.parse(range)[1]}/${
			users.count
		}`
	)

	res.json(users.rows)
}

/**
 * Get user by Id
 * @param {id: number} req
 */
const getOne = async (req, res, next) => {
	let user = await getUserById(req.params.id)

	res.json(user)
}

/**
 * Get many user with filter
 * @param {*} req
 */
const getMany = async (req, res, next) => {
	let users

	try {
		users = await getUserFilter(JSON.parse(req.query.filter))
	} catch (error) {
		console.log(error)
	}

	res.json(users)
}

module.exports = {
	getList,
	getOne,
	getMany,
}
