const { VideoDetail } = require('../../../models/')

/**
 * Get the whole list of video
 * @param [rangeStart: number, rangeEnd: number] range
 * @param [field: string, order(ASC or DESC): string] sort
 * @param {object} filter
 */
const getListVideo = async (range, sort, filter) => {
	const [field, order] = sort
	const [start, end] = range

	return await VideoDetail.findAndCountAll({
		raw: true,
		limit: end - start + 1,
		offset: start,
		order: field ? [[field, order]] : [],
		where: filter,
	})
}

/**
 * Get one report by id
 * @param {number} id
 */
const getVideoById = async (id) => {
	return await VideoDetail.findByPk(id)
}

/**
 * Get users with a filter
 * @param {object} filter
 */
const getVideoFilter = async (filter) => {
	return await VideoDetail.findAll({ where: filter })
}

module.exports = { getListVideo, getVideoById, getVideoFilter }
