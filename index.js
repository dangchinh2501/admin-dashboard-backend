const express = require('express')
const cors = require('cors')
const config = require('./config/config')
const router = require('./routes')

const app = express()
app.use(express.json())
app.use(cors())
app.use((req, res, next) => {
	res.setHeader('Access-Control-Expose-Headers', 'content-range')
	next()
})

app.use(router)

app.listen(config.PORT, () =>
	console.log(`Server is up and running on port ${config.PORT}`)
)
