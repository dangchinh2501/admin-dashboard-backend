const { sequelize } = require('../connections')
const { DataTypes } = require('sequelize')
const { sequelizedObject } = require('../controller/common')

const ReportVideos = sequelizedObject({
	userId: {
		type: DataTypes.NUMBER,
		allowNull: false,
	},
	videoId: {
		type: DataTypes.NUMBER,
		allowNull: false,
	},
	type: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	description: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	createdAt: {
		type: DataTypes.DATE,
		allowNull: false,
	},
	isDeleted: {
		type: DataTypes.NUMBER,
		allowNull: true,
	},
})

module.exports = sequelize.define('ReportVideos', ReportVideos, {
	tableName: 'report_video',
	timestamps: false,
})
