const { sequelize } = require('../connections')
const { DataTypes } = require('sequelize')
const { sequelizedObject } = require('../controller/common')

const Users = sequelizedObject({
	username: {
		type: DataTypes.STRING,
		allowNull: true,
	},
	mobileNumber: {
		type: DataTypes.STRING,
		allowNull: false,
		field: 'mobile_number',
	},
	fullName: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	email: {
		type: DataTypes.STRING,
		allowNull: true,
	},
	password: {
		type: DataTypes.STRING,
		allowNull: true,
	},
	emailVerified: {
		type: DataTypes.INTEGER,
		allowNull: true,
	},
	verificationToken: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	bio: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	dob: {
		type: DataTypes.DATE,
		allowNull: false,
	},
	profilePic: {
		type: DataTypes.STRING,
		allowNull: true,
	},
	createdAt: {
		type: DataTypes.DATE,
		allowNull: true,
	},
	updatedAt: {
		type: DataTypes.DATE,
		allowNull: false,
	},
	isDeleted: {
		type: DataTypes.DATE,
		allowNull: true,
	},
	deletedAt: {
		type: DataTypes.DATE,
		allowNull: true,
	},
	deletedBy: {
		type: DataTypes.INTEGER,
		allowNull: true,
	},
	isPaused: {
		type: DataTypes.INTEGER,
		allowNull: false,
	},
	tempId: {
		type: DataTypes.INTEGER,
		allowNull: true,
	},
})

module.exports = sequelize.define('User', Users, { tableName: 'user' })
