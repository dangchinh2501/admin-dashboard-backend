const { sequelize } = require('../connections')
const { DataTypes } = require('sequelize')
const { sequelizedObject } = require('../controller/common')

const VideoDetail = sequelizedObject({
	userId: {
		type: DataTypes.NUMBER,
		allowNull: false,
	},
	videoKey: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	thumbnailKey: {
		type: DataTypes.STRING,
		allowNull: true,
	},
	gifKey: {
		type: DataTypes.STRING,
		allowNull: true,
	},
	caption: {
		type: DataTypes.STRING,
		allowNull: true,
	},
	latitude: {
		type: DataTypes.INTEGER,
		allowNull: false,
	},
	longitude: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	category: {
		type: DataTypes.STRING,
		allowNull: true,
	},
	loops: {
		type: DataTypes.NUMBER,
		allowNull: false,
	},
	uploadTime: {
		type: DataTypes.DATE,
		allowNull: false,
	},
	shareLocation: {
		type: DataTypes.NUMBER,
		allowNull: false,
	},
	location: {
		type: DataTypes.STRING,
		allowNull: true,
	},
	postType: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	transcodingStatus: {
		type: DataTypes.BOOLEAN,
		allowNull: false,
	},
	accessibility: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	isPaused: {
		type: DataTypes.INTEGER,
		allowNull: false,
	},
	isDeleted: {
		type: DataTypes.BOOLEAN,
		allowNull: true,
	},
	deletedAt: {
		type: DataTypes.DATE,
		allowNull: true,
	},
	deletedBy: {
		type: DataTypes.NUMBER,
		allowNull: true,
	},
	address: {
		type: DataTypes.BOOLEAN,
		allowNull: true,
	},
	tempId: {
		type: DataTypes.NUMBER,
		allowNull: true,
	},
})

module.exports = sequelize.define('Videos', VideoDetail, {
	tableName: 'video_detail',
	timestamps: false,
})
