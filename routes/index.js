const userRouter = require('./user')
const videoDetailRouter = require('./video-detail')
const reportVideoRouter = require('./report-video')

const router = require('express').Router()

router.use('/user', userRouter)
router.use('/video-detail', videoDetailRouter)
router.use('/report-video', reportVideoRouter)

module.exports = router
