const router = require('express').Router()
const reportVideoController = require('../controller/report-video/report-video-controller')

router.get('/', reportVideoController.getList)
router.get('/:id', reportVideoController.getOne)
router.get('/with/filter', reportVideoController.getMany)

module.exports = router
