const router = require('express').Router()
const userController = require('../controller/user/user-controllers')

router.get('/', userController.getList)
router.get('/:id', userController.getOne)
router.get('/with/filter', userController.getMany)

module.exports = router
