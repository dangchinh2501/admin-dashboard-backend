const router = require('express').Router()
const videoDetailController = require('../controller/video-detail/video-detail-controller')

router.get('/', videoDetailController.getList)
router.get('/:id', videoDetailController.getOne)
router.get('/with/filter', videoDetailController.getMany)

module.exports = router
